# 😼Robot SuperCat Attack😼 🎮

---

Run accross the rooftops of the city as ME-0W, the Robot SuperCat 😼.
A endless runner browser game build with [phaser](https://phaser.io/).

## Play

> The game is not released yet. As soon as there is a playable version, there will be a link here.

### Controls

- Jump: Space
- Dash: D
