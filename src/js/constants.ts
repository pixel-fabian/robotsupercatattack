/* eslint-disable no-unused-vars */
export enum SCENES {
  LOAD = 'LoadScene',
  MENU = 'MenuScene',
  GAME = 'GameScene',
  GAMEOVER = 'GameOverScene',
  HIGHSCORE = 'HighscoreScene',
  CREDITS = 'CreditsScene',
}

export enum TEXTURES {
  BACKGROUND_MENU = 'background',
  BACKGROUND_BG = 'background_bg',
  BACKGROUND_MG = 'background_mg',
  BACKGROUND_FG = 'background_fg',
  PLATFORM_01 = 'platform_01',
  CAT = 'cat',
  BUTTON_PLAY = 'button_play',
  BUTTON_HIGHSCORE = 'button_highscore',
  BUTTON_CREDITS = 'button_credits',
  OBSTACLE_AIRCON = 'obstacle_aircon',
  EXPLOSION = 'explosion',
}

export enum ANIMATIONS {
  JUMP = 'jump',
  DASH = 'dash',
  RUN = 'run',
  EXPLOSION = 'explosion',
}

export const COLORS = {
  BACKGROUND: new Phaser.Display.Color(18, 3, 48, 255),
  TEXT: new Phaser.Display.Color(255, 255, 255, 255),
  PRIMARY: new Phaser.Display.Color(242, 19, 183, 255),
};

export enum AUDIO {
  MUSIC_MENU = 'music_menu',
  MUSIC_GAME = 'music_game',
  JUMP = 'jump',
}

export enum STATES {
  SINGLE_JUMP = 'singleJump',
  DOUBLE_JUMP = 'doubleJump',
  DASH = 'dash',
  RUN = 'run',
  DIE = 'die',
}
