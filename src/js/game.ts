import 'phaser';
import LoadScene from './scenes/LoadScene';
import MenuScene from './scenes/MenuScene';
import GameScene from './scenes/GameScene';
import GameOverScene from './scenes/GameOverScene';
import HighscoreScene from './scenes/HighscoreScene';
import CreditsScene from './scenes/CreditsScene';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO, // WebGL if available
  title: 'Blueprint',
  width: 800,
  height: 533,
  parent: 'game',
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
      gravity: { y: 900 },
    },
  },
  pixelArt: true,
  scene: [
    LoadScene,
    MenuScene,
    GameScene,
    GameOverScene,
    HighscoreScene,
    CreditsScene,
  ],
};

window.onload = () => {
  new Phaser.Game(config);
};
