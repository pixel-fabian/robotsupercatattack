export default class LoadingBar extends Phaser.GameObjects.Group {
  private backgroundColor = 0x222222;
  private backgroundRect: Phaser.GameObjects.Graphics;
  private barColor = 0xcccccc;
  private barRect: Phaser.GameObjects.Graphics;
  private width: number;
  private height: number;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    width: number,
    height: number,
  ) {
    super(scene);
    scene.add.existing(this);

    this.width = width;
    this.height = height;

    this.backgroundRect = new Phaser.GameObjects.Graphics(scene, {
      fillStyle: {
        color: this.backgroundColor,
      },
    });
    this.backgroundRect.fillRect(x, y, width, height);
    this.add(this.backgroundRect);

    this.barRect = new Phaser.GameObjects.Graphics(scene, {
      fillStyle: {
        color: this.barColor,
      },
    });
    this.add(this.barRect);
  }

  /**
   *
   * @param percentage Number from 0 to 1
   */
  fillBar(percentage: number) {
    const padding = 10;
    const width = this.width - padding * 2;
    const height = this.height - padding * 2;

    this.barRect.fillRect(
      this.backgroundRect.x - padding,
      this.backgroundRect.y - padding,
      width * percentage,
      height,
    );
  }
}
