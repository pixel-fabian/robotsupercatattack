export class Obstacle extends Phaser.Physics.Arcade.Image {
  private destructable = false;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    texture: string | Phaser.Textures.Texture,
  ) {
    super(scene, x, y, texture);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.setPushable(false);
  }

  getRndType() {}
}
