export default class TextButton extends Phaser.GameObjects.Text {
  private enabled = true;
  private hoverColor: string;
  private enabledColor: string;
  private disabledColor: string = '#bbbbbb';
  private onPress: Function;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    text: string,
    style: Phaser.Types.GameObjects.Text.TextStyle,
    hoverColor: string,
    onPress: Function,
  ) {
    const oStyle: Phaser.Types.GameObjects.Text.TextStyle = {
      fontFamily: 'Nunito',
      color: '#fff',
      fontSize: '16px',
      ...style,
    };
    super(scene, x, y, text, oStyle);
    scene.add.existing(this);

    this.enabledColor = oStyle.color;
    this.hoverColor = hoverColor;
    this.onPress = onPress;
    this.setEnabled(this.enabled);
  }

  setEnabled(bool: boolean) {
    this.enabled = bool;

    if (this.enabled) {
      this.setColor(this.enabledColor);
      this.setInteractive({ useHandCursor: true });
      this.on('pointerover', () => {
        this.setColor(this.hoverColor);
      });
      this.on('pointerout', () => {
        this.setColor(this.enabledColor);
      });
      this.on('pointerdown', () => {
        this.onPress();
      });
    } else {
      this.setColor(this.disabledColor);
      this.removeInteractive();
    }
  }
}
