import { TEXTURES } from '../constants';
import GameScene from '../scenes/GameScene';
import { Obstacle } from './Obstacle';
import Player from './Player';

export default class Platforms extends Phaser.Physics.Arcade.StaticGroup {
  public scene: GameScene;

  constructor(
    world: Phaser.Physics.Arcade.World,
    scene: Phaser.Scene,
    children?:
      | Phaser.GameObjects.GameObject[]
      | Phaser.Types.GameObjects.Group.GroupConfig
      | Phaser.Types.GameObjects.Group.GroupCreateConfig,
    config?:
      | Phaser.Types.GameObjects.Group.GroupConfig
      | Phaser.Types.GameObjects.Group.GroupCreateConfig,
  ) {
    super(world, scene, children, config);
    scene.add.existing(this);

    // Create first platform where the player starts
    const newPlatform = this.create(180, 500, TEXTURES.PLATFORM_01);
    newPlatform.body.setSize(760, 180, true);
  }

  spawnPlatform() {
    // Spawn one new plaform, outside of the camera
    const lastPlatform = this.getChildren()[
      this.getChildren().length - 1
    ] as Phaser.GameObjects.Sprite;
    const camRightX = this.scene.scale.width + this.scene.cameras.main.scrollX;
    if (lastPlatform.x > camRightX) return;

    const spawnX =
      lastPlatform.getRightCenter().x + Phaser.Math.Between(700, 900);
    const spawnY = Phaser.Math.Between(320, 520);
    const newPlatform = this.create(spawnX, spawnY, TEXTURES.PLATFORM_01);
    newPlatform.body.setSize(760, 180, true);
    this.maybeSpawnObstacle(newPlatform);
  }

  maybeSpawnObstacle(newPlatform) {
    const obst = new Obstacle(
      this.scene,
      newPlatform.x,
      newPlatform.y - 100,
      TEXTURES.OBSTACLE_AIRCON,
    )
      .setOrigin(1)
      .setScale(2);
    this.scene.physics.add.collider(newPlatform, obst);
    this.scene.physics.add.collider(
      obst,
      this.scene.player,
      this._collisionObstPlayer,
      null,
      this,
    );
  }

  _collisionObstPlayer(obst: Obstacle, player: Player) {
    if (player.body.touching.right) {
      player.die();
    }
    if (player.body.touching.down && player.isJumping()) {
      player.run();
    }
  }
}
