import { STATES, ANIMATIONS, TEXTURES } from '../constants';
import GameScene from '../scenes/GameScene';

export default class Player extends Phaser.Physics.Arcade.Sprite {
  public scene: GameScene;
  private acceleration = 0.2;
  private runVelocity = 350;
  private dashVelocity = 200;
  private dashDuration = 500;
  private jumpVelocity = {
    singleJump: -300,
    doubleJump: -250,
  };
  private jumpDuration = 600;
  private jumpTimer = 0;
  private jumpLocked = false;
  private doubleJumpLocked = false;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    texture: string | Phaser.Textures.Texture,
    frame?: string | number,
  ) {
    super(scene, x, y, texture, frame);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.run();
  }

  run() {
    if (this.isRunning()) return;
    this.state = STATES.RUN;
    this.play(ANIMATIONS.RUN);
    this.setVelocityX(this.runVelocity);
    this.jumpLocked = false;
  }

  accelerate() {
    this.runVelocity += this.acceleration;
  }

  jump() {
    if (!this.jumpLocked) {
      // start single jump
      this.state = STATES.SINGLE_JUMP;
      this.play(ANIMATIONS.JUMP);
      this.setVelocityY(this.jumpVelocity.singleJump);
      this.jumpTimer = this.scene.game.getTime();
      this.jumpLocked = true;
      this.doubleJumpLocked = true;
    } else if (
      this.state === STATES.SINGLE_JUMP &&
      this.doubleJumpLocked === true
    ) {
      if (this.scene.game.getTime() - this.jumpTimer > this.jumpDuration) {
        // single jump is over
        this.jumpTimer = 0;
      } else {
        // continue single jump
        this.setVelocityY(this.jumpVelocity.singleJump);
      }
    } else if (
      this.state === STATES.SINGLE_JUMP &&
      this.doubleJumpLocked != true
    ) {
      // start double jump
      this.state = STATES.DOUBLE_JUMP;
      this.play(ANIMATIONS.JUMP);
      this.setVelocityY(this.jumpVelocity.doubleJump);
      this.jumpTimer = this.scene.game.getTime();
      this.doubleJumpLocked = true;
    } else if (this.state === STATES.DOUBLE_JUMP) {
      if (this.scene.game.getTime() - this.jumpTimer > this.jumpDuration) {
        // double jump is over
        this.jumpTimer = 0;
      } else {
        // continue double jump
        this.setVelocityY(this.jumpVelocity.doubleJump);
      }
    }
  }
  unlockDoubleJump() {
    if (this.state === STATES.SINGLE_JUMP) {
      this.doubleJumpLocked = false;
    }
  }

  dash() {
    this.state = STATES.DASH;
    this.play(ANIMATIONS.DASH);
    this.setVelocityX(this.runVelocity + this.dashVelocity);
    this.scene.time.addEvent({
      delay: this.dashDuration,
      callback: () => {
        this.run();
      },
    });
  }

  die() {
    if (this.state === STATES.DIE) return;
    this.state = STATES.DIE;
    this.setVelocity(0);
    this.setVisible(false);
    this.scene.cameras.main.shake(300, 0.02);
    const explosion = this.scene.add.sprite(this.x, this.y, TEXTURES.EXPLOSION);
    explosion.play(ANIMATIONS.EXPLOSION);
    explosion.on('animationcomplete', () => {
      explosion.setVisible(false);
      this.scene.gameOver();
    });
  }

  isDead() {
    return this.state === STATES.DIE;
  }
  isJumping() {
    return (
      this.state === STATES.SINGLE_JUMP || this.state === STATES.DOUBLE_JUMP
    );
  }
  isRunning() {
    return this.state === STATES.RUN;
  }
  isDashing() {
    return this.state === STATES.DASH;
  }
}
