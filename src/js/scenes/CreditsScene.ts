import 'phaser';
import { SCENES, TEXTURES } from '../constants';

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.CREDITS,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    this.add.image(0, 0, TEXTURES.BACKGROUND_MENU).setOrigin(0, 0);
    // buttons
    const buttonPlay = this.add.text(50, 50, '< back', {
      fontFamily: 'Share Tech Mono Regular',
      color: '#fff',
      fontSize: '35px',
    });
    buttonPlay.setInteractive();
    buttonPlay.on('pointerdown', () => {
      this.scene.start(SCENES.MENU);
    });
    //text
    this.add.text(50, 330, 'A game by: pixel-fabian', {
      fontFamily: 'Share Tech Mono Regular',
      color: '#fff',
      fontSize: '32px',
    });
    this.add.text(50, 360, 'Game engine: Phaser 3 by PhotonStorm', {
      fontFamily: 'Share Tech Mono Regular',
      color: '#fff',
      fontSize: '32px',
    });
    this.add.text(50, 390, 'Music: Space Cats - Magic Fly by Enjoyker', {
      fontFamily: 'Share Tech Mono Regular',
      color: '#fff',
      fontSize: '32px',
    });
    this.add.text(
      50,
      420,
      'Font: Share Tech Mono Regular by Carrois Apostrophe (OFL)',
      {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      },
    );
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
