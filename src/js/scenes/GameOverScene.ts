import { SCENES, TEXTURES } from '../constants';
import TextButton from '../objects/TextButton';

export default class GameOverScene extends Phaser.Scene {
  private score = 0;
  private lifes = 2;

  constructor() {
    super({
      key: SCENES.GAMEOVER,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(data): void {
    this.add.image(0, 0, TEXTURES.BACKGROUND_MENU).setOrigin(0, 0);
    if (data.score) {
      this.score = data.score;
    }
    if (data.lifes) {
      this.lifes = data.lifes;
    }
    const centerX = this.scale.width / 2;
    // text
    this.add
      .text(centerX, 100, `${this.score}`, {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '64px',
      })
      .setOrigin(0.5);
    this.add
      .text(centerX, 180, `remaining lifes: ${this.lifes}`, {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      })
      .setOrigin(0.5);

    // buttons
    new TextButton(
      this,
      centerX,
      350,
      '>> continue <<',
      {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      },
      '#eee',
      () => {
        this.scene.start(SCENES.GAME, { lifes: this.lifes });
      },
    ).setOrigin(0.5);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
