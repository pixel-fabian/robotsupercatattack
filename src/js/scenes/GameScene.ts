import { SCENES, TEXTURES, AUDIO } from '../constants';
import Player from '../objects/Player';
import Platforms from '../objects/Platforms';

export default class GameScene extends Phaser.Scene {
  private platforms: Platforms;
  private obstacles: Phaser.Physics.Arcade.StaticGroup;
  public player: Player;
  private background_bg?: Phaser.GameObjects.TileSprite;
  private background_mg?: Phaser.GameObjects.TileSprite;
  private background_fg?: Phaser.GameObjects.TileSprite;

  private score = 0;
  private lifes = 3;
  private scoreText: Phaser.GameObjects.Text;
  private lifesText: Phaser.GameObjects.Text;
  private keySpace: Phaser.Input.Keyboard.Key;
  private keyD: Phaser.Input.Keyboard.Key;

  constructor() {
    super({
      key: SCENES.GAME,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(data): void {
    if (data.lifes) {
      this.lifes = data.lifes;
    }
    // sounds
    if (this.sound.get(AUDIO.MUSIC_MENU)) {
      this.sound.get(AUDIO.MUSIC_MENU).stop();
    }
    const music = this.sound.add(AUDIO.MUSIC_GAME, { loop: true });
    music.play();
    // store the width and height of the game screen
    const width = this.scale.width;
    const height = this.scale.height;
    // add background
    this.background_bg = this.add
      .tileSprite(0, 0, 800, 533, TEXTURES.BACKGROUND_BG)
      .setOrigin(0, 0)
      .setScrollFactor(0, 0);
    this.background_mg = this.add
      .tileSprite(0, 0, 800, 533, TEXTURES.BACKGROUND_MG)
      .setOrigin(0, 0)
      .setScrollFactor(0, 0);
    this.background_fg = this.add
      .tileSprite(0, 0, 800, 533, TEXTURES.BACKGROUND_FG)
      .setOrigin(0, 0)
      .setScrollFactor(0, 0);
    // create platforms
    this.platforms = new Platforms(this.physics.world, this);
    // create player
    this.player = new Player(this, 100, 150, TEXTURES.CAT);
    // add collision
    this.physics.add.collider(
      this.player,
      this.platforms,
      this._onCollidePlayerPlatform,
      null,
      this,
    );
    // make camera follow the cat
    this.cameras.main.startFollow(this.player);
    this.cameras.main.setFollowOffset(-300, 0);
    this.cameras.main.setBounds(0, 0, Number.MAX_SAFE_INTEGER, height);
    // get keys
    this.keySpace = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.SPACE,
    );
    this.keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    // text
    this.scoreText = this.add
      .text(width / 2, 15, `${this.score}`, {
        fontFamily: 'Share Tech Mono Regular',
        fontSize: '24px',
      })
      .setScrollFactor(0)
      .setOrigin(0.5);
    this.lifesText = this.add
      .text(60, 15, `lifes: ${this.lifes}`, {
        fontFamily: 'Share Tech Mono Regular',
        fontSize: '24px',
      })
      .setScrollFactor(0)
      .setOrigin(0.5);
  }

  update(): void {
    this.background_mg.tilePositionX += 0.2;
    this.background_fg.tilePositionX += 0.4;

    if (this.keySpace.isDown) {
      this.player.jump();
    } else if (Phaser.Input.Keyboard.JustUp(this.keySpace)) {
      this.player.unlockDoubleJump();
    }
    if (Phaser.Input.Keyboard.JustDown(this.keyD)) {
      this.player.dash();
    }
    this.platforms.spawnPlatform();
    if (this.player.y >= this.scale.height) {
      this.player.die();
    }
    // update score
    this._updateScore();
    this.player.accelerate();
  }

  //////////////////////////////////////////////////
  // Public methods                              //
  //////////////////////////////////////////////////

  gameOver() {
    if (this.player.isDead()) {
      this.lifes--;
      this.scene.pause();
      if (this.lifes > 0) {
        this.scene.start(SCENES.GAMEOVER, {
          score: this.score,
          lifes: this.lifes,
        });
      } else {
        this.scene.start(SCENES.HIGHSCORE, {
          score: this.score,
        });
      }
    }
  }

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  _updateScore() {
    this.score++;
    this.scoreText.setText(`${this.score}`);
  }

  _onCollidePlayerPlatform(player: Player) {
    if (player.body.touching.right) {
      this.player.die();
    }
    if (player.body.touching.down && player.isJumping()) {
      player.run();
    }
  }
}
