import { SCENES, TEXTURES, AUDIO, ANIMATIONS } from '../constants';
import LoadingBar from '../objects/LoadingBar';

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.LOAD,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {
    // add text
    this.add.text(360, 225, 'Loading...', {
      fontFamily: 'sans-serif',
      color: '#fff',
    });
    // create loading bar
    const loadingBar = new LoadingBar(this, 255, 255, 300, 40);
    this.load.on('progress', (percentage: number) => {
      loadingBar.fillBar(percentage);
    });

    // load all textures
    this.load.image(
      TEXTURES.BACKGROUND_MENU,
      'assets/img/city_sunset_gradient.png',
    );
    this.load.image(TEXTURES.BACKGROUND_BG, 'assets/img/city_sunset_bg.png');
    this.load.image(TEXTURES.BACKGROUND_MG, 'assets/img/city_sunset_mg.png');
    this.load.image(TEXTURES.BACKGROUND_FG, 'assets/img/city_sunset_fg.png');
    this.load.image(TEXTURES.PLATFORM_01, 'assets/img/building_01.png');
    this.load.image(TEXTURES.OBSTACLE_AIRCON, 'assets/img/airconditioner.png');
    this.load.spritesheet(TEXTURES.CAT, 'assets/img/robocat_sprite.png', {
      frameWidth: 100,
      frameHeight: 64,
    });
    this.load.spritesheet(TEXTURES.EXPLOSION, 'assets/img/explosion.png', {
      frameWidth: 64,
      frameHeight: 64,
    });

    // load all audio
    this.load.audio(AUDIO.MUSIC_MENU, [
      'assets/audio/space_cats_magic_fly_intro.mp3',
    ]);
    this.load.audio(AUDIO.MUSIC_GAME, [
      'assets/audio/space_cats_magic_fly_endless.mp3',
    ]);
    this.load.audio(AUDIO.JUMP, ['assets/audio/jump.wav']);
  }

  create(): void {
    this._createAnimations();
    this.scene.start(SCENES.MENU);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  _createAnimations() {
    this.anims.create({
      key: ANIMATIONS.RUN,
      frames: this.anims.generateFrameNumbers(TEXTURES.CAT, {
        start: 0,
        end: 7,
      }),
      frameRate: 10,
      repeat: -1, // -1: infinity
    });
    this.anims.create({
      key: ANIMATIONS.JUMP,
      frames: this.anims.generateFrameNumbers(TEXTURES.CAT, {
        start: 5,
        end: 5,
      }),
      frameRate: 5,
      repeat: 1,
    });
    this.anims.create({
      key: ANIMATIONS.DASH,
      frames: this.anims.generateFrameNumbers(TEXTURES.CAT, {
        start: 8,
        end: 13,
      }),
      frameRate: 10,
      repeat: 1,
    });
    this.anims.create({
      key: ANIMATIONS.EXPLOSION,
      frames: this.anims.generateFrameNumbers(TEXTURES.EXPLOSION, {
        start: 0,
        end: 7,
      }),
      repeat: 0,
      frameRate: 16,
    });
  }
}
