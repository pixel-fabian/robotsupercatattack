import { SCENES, TEXTURES, AUDIO } from '../constants';
import TextButton from '../objects/TextButton';

export default class MenuScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.MENU,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    this.add.image(0, 0, TEXTURES.BACKGROUND_MENU).setOrigin(0, 0);
    // sounds
    if (!this.sound.get(AUDIO.MUSIC_MENU)) {
      const music = this.sound.add(AUDIO.MUSIC_MENU, { loop: true });
      music.play();
    }
    // buttons
    const centerX = this.scale.width / 2;
    new TextButton(
      this,
      centerX,
      250,
      '>> play <<',
      {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      },
      '#eee',
      () => {
        this.scene.start(SCENES.GAME);
      },
    ).setOrigin(0.5);

    new TextButton(
      this,
      centerX,
      320,
      '>> highscores <<',
      {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      },
      '#eee',
      () => {
        this.scene.start(SCENES.HIGHSCORE);
      },
    ).setOrigin(0.5);

    new TextButton(
      this,
      centerX,
      390,
      '>> credits <<',
      {
        fontFamily: 'Share Tech Mono Regular',
        color: '#fff',
        fontSize: '32px',
      },
      '#eee',
      () => {
        this.scene.start(SCENES.CREDITS);
      },
    ).setOrigin(0.5);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
